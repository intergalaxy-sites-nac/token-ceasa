<!--====================================================================
                            Include All Js File 
     ====================================================================-->
<!-- All Plugins -->
<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/js/popper-v1.14.3.min.js"></script>
<script src="assets/js/bootstrap-v4.1.3.min.js"></script>
<script src="assets/js/owl.carousel.min.js"></script>
<script src="assets/js/timeline.min.js"></script>
<script src="assets/js/jquery.magnific-popup.min.js"></script>
<script src="assets/js/wow.min.js"></script>
<script src="assets/js/main.js"></script>

<script>
    function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
        document.getElementById("mySidenav").style.right = "0";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("mySidenav").style.right = "-10px";
    }
</script>
