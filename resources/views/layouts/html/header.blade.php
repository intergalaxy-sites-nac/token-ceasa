<header class="main-header">

    <!--Header-Upper-->
    <div class="header-upper">
        <div class="container-fluid clearfix">

            <div class="header-inner d-lg-flex align-items-center">

                <div class="logo-outer">
                    <div class="logo">
                        <a href="/">
                            <img src="{{ asset('assets/img/logo-ceasa.svg') }}" alt="" title="">
                        </a>
                    </div>
                </div>

                <div class="nav-outer clearfix ml-auto" id="mypushbar1">
                    <!-- Main Menu -->
                    <nav class="main-menu navbar-expand-lg">
                        <div class="navbar-header clearfix">
                            <!-- Toggle Button -->
                            <button type="button" class="navbar-toggle btn-teste" data-toggle="collapse"
                                onclick="openNav()" id="btn-nav-open-2" >
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div class="navbar-collapse collapse clearfix">
                            <ul class="navigation clearfix">
                                <li class="current"><a href="/">Home</a></li>
                                <li><a href="#about">Sobre</a></li>
                                <li><a href="#app">Aplicativo</a></li>
                                <li><a href="#whitepaper">Whitepaper</a></li>
                                <li><a href="#section-companies">Empresas</a></li>
                                <li><a href="#contact">Contato</a></li>
                            </ul>
                        </div>
                        <div id="mySidenav" class="sidenav">
                            <div class="menu-close">
                                Menu
                                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()"><i class="fas fa-times"></i></a>
                            </div>
                            <div class="menu-btn">
                                <a href="#" class="btn-bg">Login</a>
                            </div>
                            <li class="current"><a href="/">Home</a></li>
                            <li><a href="#about">Sobre</a></li>
                            <li><a href="#app">Aplicativo</a></li>
                            <li><a href="#ceasa">New Ceasa</a></li>
                            <li><a href="#benefits">Benefícios</a></li>
                            <li><a href="#contact">Contato</a></li>
                        </div>

                    </nav>
                    <!-- Main Menu End-->
                </div>

                <div class="menu-btn">
                    <a href="#" class="btn-bg">Login</a>
                </div>

            </div>

        </div>
    </div>
    <!--End Header Upper-->

</header>
