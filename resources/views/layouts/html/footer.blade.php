<footer class="footer">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <img src="{{ asset('assets/img/logo-ceasa.svg') }}" />
            </div>
            {{-- <div class="col-lg-4 text-center scroll-to-target" data-target="html">
                <img src="{{ asset('assets/img/back-to-top.svg') }}" />
            </div> --}}
            <div class="col-lg-6">
                <div class="footer-menu text-lg-right">
                    <ul>
                        <li>
                            <a href="#" class="icon icon-face">
                                <svg id="Grupo_111" data-name="Grupo 111" xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="32.825" height="32.825"
                                    viewBox="0 0 32.825 32.825">
                                    <defs>
                                        <clipPath id="clip-path">
                                            <rect id="Retângulo_86" data-name="Retângulo 86" width="32.825"
                                                height="32.825" fill="none" />
                                        </clipPath>
                                    </defs>
                                    <g id="Grupo_110" data-name="Grupo 110" transform="translate(0)"
                                        clip-path="url(#clip-path)">
                                        <path id="Caminho_143" data-name="Caminho 143"
                                            d="M16.412,0A16.412,16.412,0,1,1,0,16.412,16.412,16.412,0,0,1,16.412,0"
                                            transform="translate(0)" fill="#84bf36" />
                                        <path id="Caminho_130" data-name="Caminho 130"
                                            d="M107.936,76.023l.7-4.3h-4.972V68.45c0-1.353.363-2.315,2.3-2.315h2.872V61.8a19.148,19.148,0,0,0-3.533-.4c-3.568,0-5.93,2.357-5.93,6.357v3.966h-4.3v4.3h4.3V87.869a16.477,16.477,0,0,0,4.289.115V76.023Z"
                                            transform="translate(-85.546 -55.248)" fill="#fff" />
                                    </g>
                                </svg>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="icon icon-insta">
                                <svg id="Grupo_119" data-name="Grupo 119" xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="32.825" height="32.825"
                                    viewBox="0 0 32.825 32.825">
                                    <path id="Caminho_136" data-name="Caminho 136"
                                        d="M16.412,0A16.412,16.412,0,1,1,0,16.412,16.412,16.412,0,0,1,16.412,0"
                                        transform="translate(0 0)" fill="url(#linear-gradient)" />
                                    <path id="Caminho_140" data-name="Caminho 140"
                                        d="M16.412,0A16.412,16.412,0,1,1,0,16.412,16.412,16.412,0,0,1,16.412,0"
                                        fill="#84bf36" />
                                    <g id="Grupo_116" data-name="Grupo 116" transform="translate(0 0)">
                                        <g id="Grupo_115" data-name="Grupo 115" transform="translate(0)">
                                            <g id="Grupo_114" data-name="Grupo 114" clip-path="url(#clip-path)">
                                                <path id="Caminho_131" data-name="Caminho 131"
                                                    d="M76.359,81.917h-9.4A5.563,5.563,0,0,1,61.4,76.361v-9.4A5.563,5.563,0,0,1,66.956,61.4h9.4a5.562,5.562,0,0,1,5.556,5.556v9.4a5.562,5.562,0,0,1-5.556,5.556m-9.4-18.806a3.851,3.851,0,0,0-3.847,3.847v9.4a3.851,3.851,0,0,0,3.847,3.847h9.4a3.851,3.851,0,0,0,3.847-3.847v-9.4a3.851,3.851,0,0,0-3.847-3.847Z"
                                                    transform="translate(-55.246 -55.247)" fill="#fff" />
                                                <path id="Caminho_132" data-name="Caminho 132"
                                                    d="M117.7,122.827a5.129,5.129,0,1,1,5.129-5.129,5.135,5.135,0,0,1-5.129,5.129m0-8.548a3.419,3.419,0,1,0,3.419,3.419,3.423,3.423,0,0,0-3.419-3.419"
                                                    transform="translate(-101.285 -101.286)" fill="#fff" />
                                                <path id="Caminho_133" data-name="Caminho 133"
                                                    d="M207.552,97.646a1.175,1.175,0,1,1-1.176,1.175,1.175,1.175,0,0,1,1.176-1.175"
                                                    transform="translate(-185.69 -87.858)" fill="#fff" />
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="icon icon-yt">
                                <svg id="Grupo_118" data-name="Grupo 118" xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink" width="32.825" height="32.825"
                                    viewBox="0 0 32.825 32.825">
                                    <defs>
                                        <clipPath id="clip-path">
                                            <rect id="Retângulo_89" data-name="Retângulo 89" width="32.825"
                                                height="32.825" fill="none" />
                                        </clipPath>
                                    </defs>
                                    <path id="Caminho_141" data-name="Caminho 141"
                                        d="M16.412,0A16.412,16.412,0,1,1,0,16.412,16.412,16.412,0,0,1,16.412,0"
                                        fill="#6e6e6e" />
                                    <g id="Grupo_117" data-name="Grupo 117" clip-path="url(#clip-path)">
                                        <path id="Caminho_142" data-name="Caminho 142"
                                            d="M16.412,0A16.412,16.412,0,1,1,0,16.412,16.412,16.412,0,0,1,16.412,0"
                                            fill="#84bf36" />
                                        <path id="Caminho_135" data-name="Caminho 135"
                                            d="M81.713,94.874a4.438,4.438,0,0,0-.815-2.036,2.935,2.935,0,0,0-2.056-.869c-2.87-.208-7.177-.208-7.177-.208h-.01s-4.307,0-7.177.208a2.934,2.934,0,0,0-2.055.869,4.436,4.436,0,0,0-.815,2.036,31.087,31.087,0,0,0-.2,3.321v1.557a31.077,31.077,0,0,0,.2,3.32,4.439,4.439,0,0,0,.815,2.036,3.477,3.477,0,0,0,2.262.877c1.642.158,6.976.206,6.976.206s4.31-.006,7.181-.214a2.934,2.934,0,0,0,2.056-.869,4.438,4.438,0,0,0,.816-2.036,31.2,31.2,0,0,0,.2-3.321V98.194a31.084,31.084,0,0,0-.2-3.32m-12.43,7.033V95.6l6.061,3.162Z"
                                            transform="translate(-55.248 -82.563)" fill="#fff" />
                                    </g>
                                </svg>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!--Copyright-->
    <div class="footer-bottom pt-30 rpb-60">
        <div class="copyright">© 2021 • Desenvolvido por IntergalaxySA.</div>
    </div>
</footer>
