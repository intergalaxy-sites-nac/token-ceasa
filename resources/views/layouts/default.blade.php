<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    {{-- HEAD --}}
    @include('layouts.html.head')
    {{-- TITLE --}}
</head>

<body>

    {{-- <a class='scrolltop'>
        <div class="d-flex flex-column align-items-center">
            <svg xmlns="http://www.w3.org/2000/svg" width="12.662" height="10.841" viewBox="0 0 12.662 10.841">
                <path id="Path"
                    d="M.257.912c-.6.6-.087,2.308.879,4.142l.251.461c.13.231.266.463.406.693l.288.459.148.227.3.448.311.436.316.42c1.167,1.51,2.4,2.643,3.18,2.643.6,0,1.458-.682,2.355-1.692l.317-.37c.265-.318.531-.662.791-1.02l.31-.438.3-.449.293-.458c.144-.23.283-.462.416-.694l.259-.463.242-.459c.89-1.749,1.347-3.336.8-3.885C12.017.505,10.62.232,8.906.1L8.25.053C8.027.041,7.8.03,7.571.022L6.879,0l-.7,0C5.6,0,5.018.017,4.46.047L3.8.088A8.315,8.315,0,0,0,.347.835Z"
                    transform="translate(12.662 10.841) rotate(180)" fill="#fff" />
            </svg>
            <p>Voltar ao topo</p>
        </div>
    </a> --}}

    {{-- HEADER --}}
    @include('layouts.html.header')
    {{-- END HEADER --}}

    {{-- CONTENT --}}
    @yield('content')
    {{-- END CONTENT --}}

    {{-- FOOTER --}}
    @include('layouts.html.footer')
    {{-- END FOOTER --}}

    {{-- JS --}}
    @include('layouts.html.plugins')
    {{-- END JS --}}
</body>

</html>
