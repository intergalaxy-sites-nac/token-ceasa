@extends('layouts.default')

@section('content')

    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">

    <div class="page-wrapper">

        @include('components.header')

        @include('components.aboutUs')

        @include('components.app')

        @include('components.roadmap')

        @include('components.benefits')

        @include('components.whitepaper')

        @include('components.partnerCompanies')

        @include('components.contact')

    </div>

@endsection
