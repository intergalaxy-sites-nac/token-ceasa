<div class="contact-page pt-100 pb-100 rpt-120 rpb-60" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 d-flex flex-column align-items-center">
                <div class="contact-site">
                    <div class="contact-title">
                        <h4 class="title-bar">Como Podemos Ajudar?</h4>
                    </div>
                    <div class="contact-text">
                        <p>Você tem alguma dúvida ou sugestão?</p>
                        <p>
                            Nos mande uma mensagem, vamos <br />
                            retornar assim que possível.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="faq-form-area pb-70 rpt-60 rpb-30">
                    <div class="container container-contact">

                        <div class="row">
                            <div class="col-lg-12">
                                <form class="faq-form" name="contact_form" action="sendmail.php" method="post">
                                    <div class="row clearfix">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="text" name="name" value="" placeholder="Nome"
                                                    required="">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="email" name="email" class="required email" value=""
                                                    placeholder="Email" required="">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <input type="text" name="subject" value="" placeholder="Assunto"
                                                    required="">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <textarea name="message" class="textarea required"
                                                    placeholder="Digite sua mensagem"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-md-12 text-center">
                                            <div class="form-group">
                                                <button class="btn-bg" type="submit"
                                                    data-loading-text="Please wait...">Enviar</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
