<section class="benefits" id="benefits">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active carousel-one w-100">
                <div class="container">
                    <div class="section-title">
                        <h4 class="left-bar">Porque nos escolher?</h4>
                    </div>
                </div>
                <div class="container d-flex">
                    <div class="col-lg-6">
                        <p>
                            1. Investimento mínimo de R$1,00 <br />
                            por token, garantido que todos <br />
                            os públicos façam parte deste <br />
                            ecossistemas.
                        </p>
                    </div>
                    <div class="col-lg-6 text-center">
                        <img src="{{ asset('assets/img/carousel/img-carousel-one.png') }}" class="img-carousel-one" />
                    </div>
                </div>
            </div>
            <div class="carousel-item carousel-two w-100">
                <div class="container">
                    <div class="section-title">
                        <h4 class="left-bar">Porque nos escolher?</h4>
                    </div>
                </div>
                <div class="container d-flex">
                    <div class="col-lg-6">
                        <p>
                            2. Rentabilidade fixa <br />
                            semestral.
                        </p>
                    </div>
                    <div class="col-lg-6 text-center">
                        <img src="{{ asset('assets/img/carousel/img-carousel-two.png') }}" class="img-carousel-one" />
                    </div>
                </div>
            </div>
            <div class="carousel-item carousel-three w-100">
                <div class="container">
                    <div class="section-title">
                        <h4 class="left-bar">Porque nos escolher?</h4>
                    </div>
                </div>
                <div class="container d-flex">
                    <div class="col-lg-6">
                        <p>
                            3. Investimento democratizado, <br />
                            transparente e com <br />
                            acompanhamento em <br />
                            tempo real.
                        </p>
                    </div>
                    <div class="col-lg-6 text-center">
                        <img src="{{ asset('assets/img/carousel/img-carousel-three.png') }}" class="img-carousel-one" />
                    </div>
                </div>
            </div>
            <div class="carousel-item carousel-four w-100">
                <div class="container">
                    <div class="section-title">
                        <h4 class="left-bar">Porque nos escolher?</h4>
                    </div>
                </div>
                <div class="container d-flex">
                    <div class="col-lg-6">
                        <p>
                            4. Maior disponibilidade por meio <br />
                            do nosso aplicativo, que pode <br />
                            ser acessado de qualquer lugar <br />
                            do mundo!
                        </p>
                    </div>
                    <div class="col-lg-6 text-center">
                        <img src="{{ asset('assets/img/carousel/img-carousel-four.png') }}" class="img-carousel-one" />
                    </div>
                </div>
            </div>
            <div class="carousel-item carousel-five w-100">
                <div class="container">
                    <div class="section-title">
                        <h4 class="left-bar">Porque nos escolher?</h4>
                    </div>
                </div>
                <div class="container d-flex">
                    <div class="col-lg-6">
                        <p>
                            5. Transações realizadas com <br />
                            tecnologia Blockchain, e que garante <br />
                            a maior segurança possível com <br />
                            dados criptografados.
                        </p>
                    </div>
                    <div class="col-lg-6 text-center">
                        <img src="{{ asset('assets/img/carousel/img-carousel-five.png') }}" class="img-carousel-one" />
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <img src="{{ asset('assets/img/benefits/previous-icon.svg') }}" />
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <img src="{{ asset('assets/img/benefits/next-icon.svg') }}" />
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>
