@if ($agent->isMobile())
    <section class="hero-section bg-img">
        <div class="col-lg-6 hero-text wow customFadeInLeft">
            <p>
                Somos a primeira plataforma de <br class="br-none" />
                tokenização do setor hortigranjeiro <br class="br-none" />
                especializada em CEASAS.
            </p>
            <a href="#" class="btn-bg btn-carousel-1">Baixe nosso App!</a>
        </div>
    </section>
@else
    <div id="carouselHeaderHome" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators carousel-home">
            <li data-target="#carouselHeaderHome" data-slide-to="0" class="active"></li>
            <li data-target="#carouselHeaderHome" data-slide-to="1"></li>
            <li data-target="#carouselHeaderHome" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <section class="hero-section bg-img">
                    <div class="col-lg-6 hero-text wow customFadeInLeft">
                        <p>
                            Somos a primeira plataforma de <br class="br-none" />
                            tokenização do setor hortigranjeiro <br class="br-none" />
                            especializada em CEASAS.
                        </p>
                        <a href="#" class="btn-bg btn-carousel-1">Baixe nosso App!</a>
                    </div>
                    <div class="col-lg-6">
                        <img src="{{ asset('assets/img/hero-section/happy-guy.png') }}">
                    </div>
                </section>
            </div>
            <div class="carousel-item">
                <section class="hero-section bg-img-2">
                    <div class="col-lg-6 hero-text wow customFadeInLeft">
                        <p>
                            O uso da tecnologia aliada a <br class="br-none" />
                            mão de obra qualificada <br class="br-none" />
                        </p>
                        <a href="#" class="btn-bg btn-carousel-2">Baixe nosso App!</a>
                    </div>
                    <div class="col-lg-6">
                        <img src="{{ asset('assets/img/hero-section/img-carousel-2.png') }}">
                    </div>
                </section>
            </div>
            <div class="carousel-item">
                <section class="hero-section bg-img-3">
                    <div class="col-lg-6 hero-text wow customFadeInLeft">
                        <p>
                            Transformando todos os <br class="br-none" />
                            Centros de Distribuições do Brasil! <br class="br-none" />
                        </p>
                        <a href="#" class="btn-bg">Baixe nosso App!</a>
                    </div>
                    <div class="col-lg-6">
                        <img src="{{ asset('assets/img/hero-section/img-carousel-3.png') }}">
                    </div>
                </section>
            </div>
        </div>
    </div>
@endif
