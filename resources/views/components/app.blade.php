<!-- start shape -->
<div>
    <!--==================================================================== 
    start mobile app
=====================================================================-->

    <section class="mobile-app-section pt-30 rpt-30 rpb-0" id="app">
        <div class="container">

            <div class="row">
                <div class="col-md-6 d-md-flex align-items-end justify-content-center order-lg-2">
                    <div class="mobile-app-content wow customFadeInRight">
                        <div class="section-title">
                            <h4 class="left-bar">Aplicativo</h4>
                            <p>
                                Nosso <strong>objetivo</strong> é criar uma <br class="br-none" />
                                comunidade entre <strong>produtores, <br class="br-none" />
                                    comerciantes</strong> e <strong>clientes</strong>, com uma <br
                                    class="br-none" />
                                gestão inovadora e simplificada, <br class="br-none" />
                                por meio de um ecossistema de <br class="br-none" />
                                negócios interativo.
                            </p>
                            <p>
                                Para isto, criamos também um <br class="br-none" />
                                aplicativo para <strong>acesso, suporte</strong> e <br class="br-none" />
                                <strong>acompanhamento</strong> de todos os <br class="br-none" />
                                seus recursos.
                            </p>
                        </div>
                        @if ($agent->isMobile())
                        @else
                            <div class="share-btn-wrap">
                                <a href="#"><img src="{{ asset('assets/img/app/logo-app-store.png') }}"
                                        alt="Logo App Store" class="w-100"></a>
                                <a href="#"><img src="{{ asset('assets/img/app/logo-google-play.png') }}"
                                        alt="Logo Google Play" class="w-100"></a>
                            </div>
                            <a href="#" class="btn-bg">Baixe agora mesmo!</a>
                        @endif
                    </div>
                </div>
                <div class="col-md-6 d-flex align-items-center flex-column justify-content-center mobile-btn order-lg-1">
                    <div class="hero-mobile1 wow customFadeInDown animated">
                        <img src="{{ asset('assets/img/app/iphone-1.png') }}" alt="hero">
                    </div>
                    <div class="hero-mobile2 wow customFadeInUp animated">
                        <img src="{{ asset('assets/img/app/iphone-3.png') }}" alt="hero">
                    </div>
                    <a class="btn-play">
                        <i class="fas fa-play"></i>
                    </a>
                    @if ($agent->isMobile())
                        <div class="share-btn-wrap">
                            <a href="#"><img src="{{ asset('assets/img/app/logo-app-store.png') }}"
                                    alt="Logo App Store" class="w-100"></a>
                            <a href="#"><img src="{{ asset('assets/img/app/logo-google-play.png') }}"
                                    alt="Logo Google Play" class="w-100"></a>
                        </div>
                        <a href="#" class="btn-bg">Baixe agora mesmo!</a>
                    @endif
                </div>
            </div>
        </div>
    </section>

    <!--==================================================================== 
    end mobile app
=====================================================================-->
</div>
<!-- end shape -->
