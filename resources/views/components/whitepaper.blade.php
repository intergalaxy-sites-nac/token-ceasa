<section class="whitepaper-section" id="whitepaper">
    <div class="container">
        <div class="row justify-content-center">
            <h1 class="whitepaper-title">Whitepaper</h1>
        </div>
        <div class="row">
            <div class="col-lg-9 whitepaper-description">
                <h2>A nova face do sistema alimentar brasileiro</h2>
                <p>
                    Acreditamos que a tecnologia é o caminho para a sustentabilidade e que <br />
                    esse avanço expande o mercado, criando um ecossistema de negócio <br />
                    <strong>transparente, confiável</strong> e <strong>descentralizado</strong>. Apoia produtores e
                    <br />
                    comerciantes, democratizando o acesso a todos os públicos.
                </p>
                <p>
                    Esse novo conceito de centrais de abastecimento representa a <i>união</i> <br />
                    da tecnologia com a mão de obra qualificada. Expressa uma <strong>forma de <br />
                        consumo consciente</strong>, onde centraliza em um único local, uma grande <br />
                    diversidade alimentar, que é encontrada apenas no Brasil.
                </p>
                <p>
                    Apostamos nessa pluralidade de ideias, pois acreditamos que a <br />
                    segurança e acessibilidade atreladas à rentabilidade <br />
                    geram maior liquidez.
                </p>
                <p>
                    Quer saber mais sobre a solução que apresntamos? Faça o <br />
                    download do nosso <strong>Whitepaper</strong>.
                </p>
            </div>
            <div class="col-lg-3">
                <img src="{{ asset('assets/img/whitepaper/img-whitepaper.png') }}" class="whitepaper-img" />
            </div>
        </div>
        <div class="row justify-content-center">
            <button class="btn-download">
                Baixe nosso Whitepaper
                <svg xmlns="http://www.w3.org/2000/svg" width="19.785" height="23.742" viewBox="0 0 19.785 23.742">
                    <path id="Icon_ionic-md-download" data-name="Icon ionic-md-download"
                        d="M19.785,8.378H14.134V0H5.651V8.378H0l9.892,9.775ZM0,20.947v2.795H19.785V20.947Z"
                        fill="#fff" />
                </svg>
            </button>
        </div>
        <img src="{{ asset('assets/img/whitepaper/balls.png') }}" class="balls-one" />
        <img src="{{ asset('assets/img/whitepaper/balls.png') }}" class="balls-two" />
    </div>
</section>
