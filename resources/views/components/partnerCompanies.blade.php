<section class="companies-section" id="section-companies">
    <div class="container">
        <div class="row flex-column">
            <h1 class="title">Empresas parceiras</h1>
            <p class="p-header">
                O ativo Ceatoken é um <strong>recurso</strong> que provém da tecnologia e moderna infraestrutura da
                <strong>New Ceasa</strong>. Quando
                implantada na Central de Abastecimento - <strong>CEASA</strong> - parceira, <strong>remodela todo o
                    processo</strong> administrativo e
                logístico, desde os produtores, comerciantes até os consumidores. Toda sua implantação conduz à
                <strong>uma
                    nova cultura de consumo</strong> para o setor hortigranjeiro brasileiro.
            </p>
        </div>
        <div class="row position-relative">
            <div class="col-lg-6 p-0">
                <p class="p-description">
                    E, para promover ainda mais a <br class="br-none" />
                    sustentabilidade, o Ceatoken <br class="br-none" />
                    se associou ao grupo TIS. O <br class="br-none" />
                    <strong>Instituto Tecnológico de <br class="br-none" />
                        Sustentabilidade</strong> reune <br class="br-none" />
                    projetos que tem como <br class="br-none" />
                    propósito - direto e indireto - <br class="br-none" />
                    a <strong>atenção, preservação</strong> e <br class="br-none" />
                    <strong>cultivo</strong> dos recursos naturais. <br class="br-none" />
                    Assim, o equilibrio entre <br class="br-none" />
                    necessidades humanas e <br class="br-none" />
                    recursos é mantido, não <br class="br-none" />
                    somente para esta geração, <br class="br-none" />
                    mas também para as gerações <br class="br-none" />
                    futuras: cujo qual, é a essência <br class="br-none" />
                    da <strong>sustentabilidade</strong>.
                </p>
            </div>
            <div class="col-lg-6 position-relative">
                @if ($agent->isMobile())
                    <img src="{{ asset('assets/img/partnerCompanies/macbook-mobile.png') }}" />
                @else
                    <img src="{{ asset('assets/img/partnerCompanies/macbook-partners.png') }}"
                        class="img-macbook" />
                    <a><img src="{{ asset('assets/img/partnerCompanies/logo-new-ceasa.png') }}"
                            class="img-ceasa" /></a>
                    <a><img src="{{ asset('assets/img/partnerCompanies/logo-tis.png') }}"
                            class="img-tis" /></a>
                @endif
            </div>
            <img src="{{ asset('assets/img/partnerCompanies/xis.png') }}" class="xis-one" />
            <img src="{{ asset('assets/img/partnerCompanies/xis-two.png') }}" class="xis-two" />
        </div>
    </div>
</section>
