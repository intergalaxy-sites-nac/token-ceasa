<!-- start shape -->
<div>
    <!--==================================================================== 
                        start about section one
=====================================================================-->

    <section class="about-us-area" id="about">
        <div class="container">
            <div class="row pb-5 pt-5">
                <div class="col-md-6 d-md-flex align-items-center order-1 order-md-1 wow customFadeInLeft">
                    <div class="about-content">
                        <div class="section-title p-0">
                            <h4 class="left-bar">Sobre</h4>
                            <h5>A expansão do ramo hortigranjeiro</h5>
                            <div class="scroll">
                                <p>
                                    Acreditamos que a tecnologia é o caminho para a
                                    sustentabilidade e que esse avanço expande o
                                    mercado, criando um ecossistema de negócio
                                    <strong>transparente</strong>, <strong>confiável</strong> e <strong>descentralizado</strong>. Apoia
                                    produtores e comerciantes, democratizando o acesso a
                                    todos os públicos.
                                </p>
                                <p>
                                    O <strong>Ceatoken</strong> é um <strong>ativo lastreado nas CEASAS</strong>, que tem
                                    como objetivo garantir acessibilidade para comprar e
                                    investir no mercado do setor hortigranjeiro.
                                </p>
                                <p>
                                    Tudo isso torna o Ceatoken <strong>a primeira plataforma de
                                    tokenização do setor alimentício</strong> especializada nas
                                    CEASAS (Centrais Estaduais de Abastecimento).
                                </p>
                            </div>
                        </div>
                        <a href="#whitepaper" class="btn-bg">Saiba mais!</a>
                    </div>
                </div>
                <div class="col-md-6 d-md-flex align-items-center order-2 order-md-2 wow customFadeInRight">
                    <div class="about-image">
                        <img src="{{ asset('assets/img/aboutUs/token.png') }}" class="img-token">
                        <img src="{{ asset('assets/img/aboutUs/img-about-us.png') }}" class="img-stars" />
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--==================================================================== 
                        end about section one
=====================================================================-->
</div>
<!-- end shape -->
