<div class="pt-80 rpt-50">

    <section class="road-map-section">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8 p-0">
                    <div class="section-title">
                        <h4 class="left-bar">O Nosso Roadmap</h4>
                    </div>
                </div>
            </div>
            <div class="timeline">
                <div class="timeline__wrap">
                    <div class="timeline__items">
                        <div class="timeline__item first">
                            <div class="timeline__content">
                                <h5>Abertura de Vendas</h5>
                                <p>
                                    O projeto disponibiliza <strong>200 milhões de
                                        ativos</strong> para a entrada de participantes
                                    por meio da Ceatoken
                                </p>
                                @if ($agent->isMobile())
                                    <img src="{{ asset('assets/img/roadmap/roadmap-mobile-1.png') }}" class="roadmap-mobile-1" />
                                @else
                                    <img src="{{ asset('assets/img/roadmap/roadmap-1.png') }}"
                                        class="body-img" />
                                @endif
                            </div>
                        </div>
                        <div class="timeline__item second">
                            <div class="timeline__content">
                                <h5>Lançamento da Plataforma</h5>
                                <p>
                                    Após o período de captação de novos
                                    parceiros, é empregada uma gestão
                                    inovadora de ecossistema digital
                                </p>
                                @if ($agent->isMobile())
                                    <img src="{{ asset('assets/img/roadmap/roadmap-mobile-2.png') }}" class="roadmap-mobile-2" />
                                @else
                                    <img src="{{ asset('assets/img/roadmap/roadmap-2.png') }}"
                                        class="body-img-2" />
                                @endif
                            </div>
                        </div>
                        <div class="timeline__item third">
                            <div class="timeline__content">
                                <h5>Desenvolvimento</h5>
                                <p>
                                    O Ceatoken terá a opção de ser utilizado <br />
                                    como meio de pagamento na <strong>New Ceasa</strong>. <br />
                                    Como o Ceatoken, o usuário terá acesso <br />
                                    ao nosso Clube de Fidelidade
                                </p>
                                @if ($agent->isMobile())
                                    <img src="{{ asset('assets/img/roadmap/roadmap-mobile-3.png') }}" class="roadmap-mobile-3" />
                                @else
                                    <img src="{{ asset('assets/img/roadmap/roadmap-3.png') }}"
                                        class="body-img-3" />
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>
