

(function ($) {
"use strict";


    // Loading Box (Preloader)
    function handlePreloader() {
        if ($('.preloader').length) {
            $('.preloader').delay(200).fadeOut(500);
        }
    }


    // Header Style and Scroll to Top
    function headerStyle() {
        if ($('.main-header').length) {
            var windowpos = $(window).scrollTop();
            var siteHeader = $('.main-header');
            var scrollLink = $('.scroll-top');
            if (windowpos >= 150) {
                // siteHeader.addClass('fixed-header');
                scrollLink.fadeIn(300);
            } else {
                siteHeader.removeClass('fixed-header');
                scrollLink.fadeOut(300);
            }
        }
    }

    headerStyle();

            // dropdown menu

        var mobileWidth = 992;
        var navcollapse = $('.navigation li.dropdown');

        navcollapse.hover(function () {
            if ($(window).innerWidth() >= mobileWidth) {
                $(this).children('ul').stop(true, false, true).slideToggle(300);
                $(this).children('.megamenu').stop(true, false, true).slideToggle(300);
            }
        });


        //Submenu Dropdown Toggle
        if ($('.main-header .navigation li.dropdown ul').length) {
            $('.main-header .navigation li.dropdown').append('<div class="dropdown-btn"><span class="fa fa-angle-down"></span></div>');

            //Dropdown Button
            $('.main-header .navigation li.dropdown .dropdown-btn').on('click', function () {
                $(this).prev('ul').slideToggle(500);
                $(this).prev('.megamenu').slideToggle(800);
            });

            //Disable dropdown parent link
            $('.navigation li.dropdown > a').on('click', function (e) {
                e.preventDefault();
            });
        }

        //Submenu Dropdown Toggle
        if ($('.main-header .main-menu').length) {
            $('.main-header .main-menu .navbar-toggle').click(function () {
                $(this).prev().prev().next().next().children('li.dropdown').hide();
            });

        }


    new WOW().init();

    //=============================================================       0          ================================


    //Accordion Box start
    if($('.accordion-box').length){
        $(".accordion-box").on('click', '.accord-btn', function() {
        
            if($(this).hasClass('active')!==true){
            $('.accordion .accord-btn').removeClass('active');
            
            }
            
            if ($(this).next('.accord-content').is(':visible')){
                $(this).removeClass('active');
                $(this).next('.accord-content').slideUp(300);
            }else{
                $(this).addClass('active');
                $('.accordion .accord-content').slideUp(300);
                $(this).next('.accord-content').slideDown(300); 
            }
        }); 
    }

    //Accordion Box start

    // magnificPopup start
    $('.popup-youtube').magnificPopup({
        type: 'video',
    });

    // magnificPopup end

    /*========== Start Proad-map-section Js ==========*/
    if($('.road-map-section').length){

        timeline(document.querySelectorAll('.timeline'), {
            forceVerticalMode: 992,
            mode: 'horizontal',
            verticalStartPosition: 'left',
            visibleItems: 8
        });

    }
    /*========== end road-map-section Js ==========*/


// Scroll to a Specific Div
    if ($('.scroll-to-target').length) {
        $(".scroll-to-target").on('click', function () {
            var target = $(this).attr('data-target');
            // animate
            $('html, body').animate({
                scrollTop: $(target).offset().top
            }, 1000);

        });
    }
    /* ==========================================================================
       When document is scroll, do
       ========================================================================== */

    $(window).on('scroll', function () {
        headerStyle();
    });

    /* ==========================================================================
       When document is loaded, do
       ========================================================================== */

    $(window).on('load', function () {
        handlePreloader();
    });

    $(document).ready(function() {
        // Show or hide the sticky footer button
        $(window).scroll(function() {
            if ($(this).scrollTop() > 200) {
                $('.scrolltop').fadeIn(200);
            } else {
                $('.scrolltop').fadeOut(200);
            }
        });
        
        // Animate the scroll to top
        $('.scrolltop').click(function(event) {
            event.preventDefault();
            
            $('html, body').animate({scrollTop: 0}, 300);
        })
    });


})(jQuery);